﻿using BasketWeight.Logic;
using System.Collections.Generic;
using System.Linq;

namespace BasketWeight.Models
{
    public class CheaterPlayer : IPlayer
    {
        private readonly List<Guess> _gameGuesses;

        public string Name { get; private set; }
        public PlayerType Type
        {
            get
            {
                return PlayerType.CheaterPlayer;
            }
        }

        public CheaterPlayer(string name, List<Guess> gameGuesses)
        {
            Name = name;
            _gameGuesses = gameGuesses;
        }

        public int DoGuess()
        {
            var guessFrom = Constants.AllWeights.Except(_gameGuesses.Select(i => i.Number));
            var randomIndex = StaticRandom.Next(0, guessFrom.Count());
            return guessFrom.ElementAt(randomIndex);
        }
    }
}
