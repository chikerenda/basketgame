﻿namespace BasketWeight.Models
{
    public class Guess
    {
        public IPlayer Player { get; private set; }
        public int Number { get; private set; }

        public Guess(IPlayer player, int number)
        {
            Player = player;
            Number = number;
        }

        public override string ToString()
        {
            return string.Format("Player {0}({1}) guessed {2}", Player.Name, Player.Type, Number);
        }
    }
}
