﻿using System.Collections.Generic;
using System.Linq;

namespace BasketWeight
{
    public static class Constants
    {
        public const int MinBasketWeight = 41;
        public const int MaxBasketWeight = 139;
        public const int MaxGuesses = 100;
        public const int ForceQuitInterval = 1500;

        public static IEnumerable<int> AllWeights = Enumerable.Range(Constants.MinBasketWeight, Constants.MaxBasketWeight - Constants.MinBasketWeight + 1);
    }
}
