﻿namespace BasketWeight.Models
{
    public class ThoroughPlayer : IPlayer
    {
        private int _guessesNumber;

        public string Name { get; private set; }
        public PlayerType Type
        {
            get
            {
                return PlayerType.ThoroughPlayer;
            }
        }

        public ThoroughPlayer(string name)
        {
            Name = name;
        }

        public int DoGuess()
        {
            return Constants.MinBasketWeight + _guessesNumber++;
        }
    }
}