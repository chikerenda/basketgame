﻿using System;

namespace BasketWeight.Logic
{
    public static class StaticRandom
    {
        [ThreadStatic]
        private static Random _random;

        public static int Next(int minValue, int maxValue)
        {
            if (_random == null)
            {
                _random = new Random(Guid.NewGuid().GetHashCode());
            }
            return _random.Next(minValue, maxValue);
        }
    }
}