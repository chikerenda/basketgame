﻿using BasketWeight.Logic;

namespace BasketWeight.Models
{
    public class RandomPlayer : IPlayer
    {
        public string Name { get; private set; }
        public PlayerType Type
        {
            get
            {
                return PlayerType.RandomPlayer;
            }
        }

        public RandomPlayer(string name)
        {
            Name = name;
        }

        public int DoGuess()
        {
            return StaticRandom.Next(Constants.MinBasketWeight, Constants.MaxBasketWeight + 1);
        }
    }
}