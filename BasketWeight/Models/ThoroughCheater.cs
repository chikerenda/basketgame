﻿using System.Collections.Generic;
using System.Linq;

namespace BasketWeight.Models
{
    public class ThoroughCheater : IPlayer
    {
        private int _lastGuess;
        private readonly List<Guess> _gameGuesses;

        public string Name { get; private set; }
        public PlayerType Type
        {
            get
            {
                return PlayerType.ThoroughCheater;
            }
        }

        public ThoroughCheater(string name, List<Guess> gameGuesses)
        {
            Name = name;
            _gameGuesses = gameGuesses;
            _lastGuess = Constants.MinBasketWeight - 1;
        }

        public int DoGuess()
        {
            var guessFrom = Constants.AllWeights.Except(_gameGuesses.Select(i => i.Number));
            var guessIndex = guessFrom.TakeWhile(i => i <= _lastGuess).Count();
            _lastGuess = guessFrom.ElementAt(guessIndex);
            return _lastGuess;
        }
    }
}