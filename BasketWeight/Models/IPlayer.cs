﻿namespace BasketWeight.Models
{
    public interface IPlayer
    {
        string Name { get; }
        PlayerType Type { get; }

        int DoGuess();
    }

    public enum PlayerType
    {
        RandomPlayer = 0,
        MemoryPlayer = 1,
        ThoroughPlayer = 2,
        CheaterPlayer = 3,
        ThoroughCheater = 4,
    }
}