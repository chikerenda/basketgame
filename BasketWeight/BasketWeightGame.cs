﻿using BasketWeight.Logic;
using BasketWeight.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace BasketWeight
{
    public class BasketWeightGame
    {
        public List<IPlayer> Players { get; private set; }
        private List<Guess> _guesses;
        private int _basketWeight;
        private bool _isStopped = true;


        public BasketWeightGame(IReadOnlyCollection<string> playersInput)
        {
            if (playersInput == null)
            {
                throw new ArgumentNullException("playersInput");
            }
            if (playersInput.Count < 2 || playersInput.Count > 8)
            {
                throw new ArgumentOutOfRangeException("playersInput", "The amount of players should be between 2 and 8");
            }
            _guesses = new List<Guess>();
            var players = playersInput.Select(InitPlayerFromString).ToList();
            // Check that each player has unique name
            if (players.Select(p => p.Name).Distinct().Count() != players.Count())
            {
                throw new ArgumentException("Each player must have a unique name.");
            }
            Players = players;
        }

        public void Start()
        {
            _isStopped = false;
            Console.WriteLine("Game starts now");
            _basketWeight = StaticRandom.Next(Constants.MinBasketWeight, Constants.MaxBasketWeight + 1);
            Console.WriteLine("Basket weight is " + _basketWeight + "kg");
            foreach (var player in Players)
            {
                // Create foregrount thred for each player
                new Thread(PlayOnePlayer).Start(player);
            }
            // Start a background thread that will stop the game in the defined interval.
            new Thread(StopGameInInterval) { IsBackground = true }.Start();
        }

        private void Stop()
        {
            if (_isStopped)
            {
                return;
            }
            _isStopped = true;
            Console.WriteLine(GetWinnerText());
        }

        private void AddGuess(IPlayer player, int number)
        {
            var guess = new Guess(player, number);
            _guesses.Add(guess);
            if (guess.Number == _basketWeight || _guesses.Count >= Constants.MaxGuesses)
            {
                Stop();
            }
        }

        private void PlayOnePlayer(object player)
        {
            if (!(player is IPlayer))
            {
                Console.WriteLine("Thread was started with wrong data.");
                return;
            }
            var currentPlayer = player as IPlayer;
            while (!_isStopped)
            {
                int guessNumber;
                lock (_guesses)
                {
                    if (_isStopped)
                    {
                        break;
                    }
                    guessNumber = currentPlayer.DoGuess();
                    AddGuess(currentPlayer, guessNumber);
                }
                Thread.Sleep(Math.Abs(guessNumber - _basketWeight));
            }
        }

        private void StopGameInInterval()
        {
            Thread.Sleep(Constants.ForceQuitInterval);
            if (!_isStopped)
            {
                Stop();
            }
        }

        private IPlayer InitPlayerFromString(string playerString)
        {
            var splittedData = playerString.Split(' ');
            if (splittedData.Length != 2)
            {
                throw new ArgumentException("Input should be in format \"NAME TYPE\"");
            }
            var playerName = splittedData[0];
            if (string.IsNullOrEmpty(playerName))
            {
                throw new ArgumentException("Name shouldn't be empty");
            }
            var playerTypeString = splittedData[1];
            PlayerType playerType;
            if (Enum.TryParse(playerTypeString, true, out playerType))
            {
                switch (playerType)
                {
                    case PlayerType.RandomPlayer:
                        return new RandomPlayer(playerName);
                    case PlayerType.MemoryPlayer:
                        return new MemoryPlayer(playerName);
                    case PlayerType.ThoroughPlayer:
                        return new ThoroughPlayer(playerName);
                    case PlayerType.CheaterPlayer:
                        return new CheaterPlayer(playerName, _guesses);
                    case PlayerType.ThoroughCheater:
                        return new ThoroughCheater(playerName, _guesses);
                    default:
                        throw new ArgumentException("The type " + playerTypeString + " does not exist");
                }
            }
            else
            {
                throw new ArgumentException("The type " + playerTypeString + " does not exist");
            }
        }

        private string GetWinnerText()
        {
            var winningGuess = _guesses.FirstOrDefault(g => g.Number == _basketWeight);
            if (winningGuess == null)
            {
                // Noone guessed the right number.
                // Get the the first closest guess and other closest guesses.
                var otherClosestGuesses = new List<Guess>();
                foreach (var guess in _guesses)
                {
                    if (winningGuess == null)
                    {
                        winningGuess = guess;
                        otherClosestGuesses = new List<Guess>();
                    }
                    else if (Math.Abs(guess.Number - _basketWeight) == Math.Abs(winningGuess.Number - _basketWeight))
                    {
                        otherClosestGuesses.Add(guess);
                    }
                    else if (Math.Abs(guess.Number - _basketWeight) < Math.Abs(winningGuess.Number - _basketWeight))
                    {
                        winningGuess = guess;
                        otherClosestGuesses = new List<Guess>();
                    }
                }

                return GenerateWinnerText(false, winningGuess, otherClosestGuesses);
            }
            else
            {
                // Someone guessed the right number.
                return GenerateWinnerText(true, winningGuess);
            }
        }

        private string GenerateWinnerText(bool isGuessedRightNumber, Guess winningGuess, List<Guess> otherClosestGuesses = null)
        {
            var resultString =
                string.Format(
                    "Player {0}({1}) wins because he guessed ",
                    winningGuess.Player.Name,
                    winningGuess.Player.Type);
            resultString +=
                isGuessedRightNumber ?
                "the right number." :
                string.Format(
                    "the closest number (his guess is - {0}).{1}{2}",
                    winningGuess.Number,
                    Environment.NewLine,
                    otherClosestGuesses != null && otherClosestGuesses.Any() ?
                        "He was the first. But there were other guesses close the same:" + Environment.NewLine +
                            string.Join(";" + Environment.NewLine, otherClosestGuesses.Select(g => g.ToString())) :
                        string.Empty);
            resultString += Environment.NewLine + "Total number of attempts is " + _guesses.Count();
            return resultString;
        }
    }
}
