﻿using BasketWeight.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BasketWeight
{
    class Program
    {
        private const string NotIntegerMessage = "Please enter an integer value between 2 and 8";
        private const string NotInRangeMessage = "Please enter a number between 2 and 8";
        private const string GameTypesMessage = "The game has 5 types of players:";
        private const string EnterPlayersMessage = "Enter of players in format \"NAME TYPE\":";
        private const string EachNameShouldBeUnique = "The name for each player chould be unique.";
        private const string PlayersWrongFormatMessage = "Wrong data format, or the player's names are not unique.";

        static void Main(string[] args)
        {
            int playersNumber;
            // Reading the number of players
            while (true)
            {
                Console.Write("Enter the number of players: ");
                var numberAsString = Console.ReadLine();
                var isConverted = int.TryParse(numberAsString, out playersNumber);
                if (!isConverted)
                {
                    Console.WriteLine(NotIntegerMessage);
                    continue;
                }
                if (playersNumber < 2 || playersNumber > 8)
                {
                    Console.WriteLine(NotInRangeMessage);
                    continue;
                }
                break;
            }

            var players = new List<string>();
            BasketWeightGame game;
            // Read players
            Console.WriteLine(GameTypesMessage + Environment.NewLine +
                              string.Join(Environment.NewLine,
                                          Enum.GetValues(typeof(PlayerType))
                                            .OfType<PlayerType>()
                                            .Select(n => string.Format("{0:g}({0:d})", n))) +
                              Environment.NewLine + EachNameShouldBeUnique +
                              Environment.NewLine + EnterPlayersMessage);
            while (true)
            {
                players.Add(Console.ReadLine());

                if (players.Count == playersNumber)
                {
                    try
                    {
                        game = new BasketWeightGame(players);
                    }
                    catch (ArgumentException e)
                    {
                        players = new List<string>();
                        Console.WriteLine(e.Message);
                        continue;
                    }
                    break;
                }
            }
            // Start the game
            game.Start();
        }
    }
}