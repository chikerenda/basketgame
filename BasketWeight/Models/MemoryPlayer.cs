﻿using BasketWeight.Logic;
using System.Collections.Generic;
using System.Linq;

namespace BasketWeight.Models
{
    public class MemoryPlayer : IPlayer
    {
        private readonly List<int> _guesses = new List<int>();

        public string Name { get; private set; }
        public PlayerType Type
        {
            get
            {
                return PlayerType.MemoryPlayer;
            }
        }

        public MemoryPlayer(string name)
        {
            Name = name;
        }

        public int DoGuess()
        {
            var guessFrom = Constants.AllWeights.Except(_guesses);
            var randomIndex = StaticRandom.Next(0, guessFrom.Count());
            var randomElement = guessFrom.ElementAt(randomIndex);
            _guesses.Add(randomElement);
            return randomElement;
        }
    }
}